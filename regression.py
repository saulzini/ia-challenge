import csv
import numpy as np
from sklearn.svm import SVR
import matplotlib.pyplot as plt

precios = []
fechas = []


def obtenerInformacion(archivo):
	with open (archivo, 'r') as archivoCSV:
		lectorCSV = csv.reader(archivoCSV)
		next(lectorCSV)
		for linea in lectorCSV:
			fechas.append(int (linea[0].split('-')[0])) #solo el numero del dia
			precios.append(float(linea[1]))
	return

def predecirPrecio (fechas ,precios , x):
	fechas = np.reshape(fechas, (len(fechas),1)) #Reconviriendo en un arrelgo
	#modelo lineal
	svr_lin = SVR(kernel='linear', C=1e3)
	#modelo polinomial
	svr_poly = SVR(kernel='poly', C=1e3 , degree=2)
	#modelo rbf
	svr_rbf = SVR(kernel='rbf', C=1e3 , gamma=0.1)

	svr_lin.fit(fechas,precios)
	svr_poly.fit(fechas,precios)
	svr_rbf.fit(fechas,precios)

	#dibujar en tabla
	plt.scatter(fechas,precios,color = 'black',label='Informacion')
	plt.plot(fechas,svr_rbf.predict(fechas) , color= 'red' , label='Modelo RBF' )
	plt.plot(fechas,svr_lin.predict(fechas) , color= 'green' , label='Modelo Lineal' )
	plt.plot(fechas,svr_poly.predict(fechas) , color= 'blue' , label='Modelo Polinomial' )
	plt.xlabel('DATE')
	plt.ylabel('PRICE')
	plt.title('SUPPORT VECTOR REGRESSION')
	plt.legend()
	plt.show()

	return svr_rbf.predict(x)[0], svr_lin.predict(x)[0], svr_poly.predict(x)[0]


obtenerInformacion('aapl.csv')
precioPredecido = predecirPrecio(fechas,precios,14)

print(precioPredecido)